package com.example.androidmvcnetcoreapiexample;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.androidmvcnetcoreapiexample.Model.User;
import com.example.androidmvcnetcoreapiexample.Remote.IMyAPI;
import com.example.androidmvcnetcoreapiexample.Remote.RetrofitClient;

import dmax.dialog.SpotsDialog;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    DatabaseHelper db;


    IMyAPI iMyAPI;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    EditText users,pass;
    TextView acc;
    Button btn_;


    //
    int masuk = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        db = new DatabaseHelper(this);




        iMyAPI = RetrofitClient.getInstance().create(IMyAPI.class);

        users = findViewById(R.id.edt_name);
       // email=findViewById(R.id.e);
        pass = findViewById(R.id.edt_user_password);

        acc = findViewById(R.id.tv2);

        btn_ = findViewById(R.id.btn);

        acc.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(MainActivity.this,RegisterActivity.class));
            }
        });

        btn_.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final AlertDialog dialog = new SpotsDialog.Builder().setContext(MainActivity.this).build();

                dialog.show();

                //.net login
                User user = new User(users.getText().toString(),pass.getText().toString(),"");


                compositeDisposable.add(iMyAPI.loginUser(user)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<String>()
                        {
                            @Override
                            public void accept(String s) throws Exception
                            {
                                Toast.makeText(MainActivity.this, s, Toast.LENGTH_LONG).show();
                                dialog.dismiss();

                                Intent i = new Intent(MainActivity.this,ProfileActivity.class);
                                startActivity(i);

                                masuk = 1+1;

                            }
                        }, new io.reactivex.functions.Consumer<Throwable>()
                        {
                            @Override
                            public void accept(Throwable throwable) throws Exception
                            {

                                dialog.dismiss();
                                Toast.makeText(MainActivity.this, throwable.getMessage(), Toast.LENGTH_LONG).show();

                            }
                        }));




              //  String s


                //sql

                String username_sql = users.getText().toString();
                String password_sql = pass.getText().toString();

                Boolean usernamepassword = db.usernamepassword(username_sql,password_sql);

                if(usernamepassword == true)
                {
                    Toast.makeText(getApplicationContext(), "tahniah", Toast.LENGTH_LONG).show();

                    if(masuk==0){
                        Toast.makeText(getApplicationContext(), "server tak on tak bole masuk bro", Toast.LENGTH_LONG).show();

                    }
                }
                else
                    Toast.makeText(getApplicationContext(),"tak berjaya masuk, salah password atau username !",Toast.LENGTH_LONG).show();


            }



        });

    }



    @Override
    protected void onStop()
    {
        compositeDisposable.clear();
        super.onStop();
    }
}
