package com.example.androidmvcnetcoreapiexample;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.androidmvcnetcoreapiexample.Model.User;
import com.example.androidmvcnetcoreapiexample.Remote.IMyAPI;
import com.example.androidmvcnetcoreapiexample.Remote.RetrofitClient;

import dmax.dialog.SpotsDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class RegisterActivity extends AppCompatActivity {

    DatabaseHelper db;

    IMyAPI iMyAPI;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    EditText users,pass,email,repass;

    Button btn_create;


    @Override
    protected void onStop()
    {
        compositeDisposable.clear();
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        db = new DatabaseHelper(this);


        iMyAPI = RetrofitClient.getInstance().create(IMyAPI.class);

        repass = findViewById(R.id.edt_user_password_confirm);
        users = findViewById(R.id.edt_name);
        email=findViewById(R.id.e);
        pass = findViewById(R.id.edt_user_password);



        btn_create = findViewById(R.id.create);


        btn_create.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final AlertDialog dialog = new SpotsDialog.Builder().setContext(RegisterActivity.this).build();

                dialog.show();





//                sqllite code
                String email_sql = email.getText().toString();
                String name_sql = users.getText().toString();
                String password_sql = pass.getText().toString();
                String re_pass_sql = repass.getText().toString();

                if (email_sql.equals("") && name_sql.equals("") && password_sql.equals("") && re_pass_sql.equals("") )
                {
                    Toast.makeText(getApplicationContext(),"Field is empty", Toast.LENGTH_LONG).show();


                }
                else
                {
                    if (password_sql.equals(re_pass_sql))
                    {
                        Boolean chkemail = db.chkemail(email_sql);
                        if(chkemail == true)
                        {
                            long insert1 = db.insert(name_sql,email_sql,password_sql);
//                            Toast.makeText(RegisterActivity.this,"value:"+insert1,Toast.LENGTH_LONG).show();
                            if(insert1 > 0)
                            {
                                Toast.makeText(getApplicationContext(),"register JADI",Toast.LENGTH_LONG).show();

                            }

                            else
                            {
                                Toast.makeText(getApplicationContext(),"Email Already exists",Toast.LENGTH_LONG).show();
                            }
                        }
                        else
                            Toast.makeText(RegisterActivity.this,"email tak masuk", Toast.LENGTH_LONG).show();

                    }
                    else
                        Toast.makeText(getApplicationContext(),"password not same",Toast.LENGTH_LONG).show();
                }



                //.net code for register
                User user = new User(users.getText().toString(),email.getText().toString(),pass.getText().toString(),"");


                compositeDisposable.add(iMyAPI.registerUser(user)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<String>()
                        {
                            @Override
                            public void accept(String s) throws Exception
                            {
                                if (s.contains("successful"))
                                {
                                     finish();
                                }
                               // Toast.makeText(RegisterActivity.this, s, Toast.LENGTH_LONG).show();

                                dialog.dismiss();
                            }
                        }, new io.reactivex.functions.Consumer<Throwable>()
                        {
                            @Override
                            public void accept(Throwable throwable) throws Exception
                            {
                                dialog.dismiss();
                               // Toast.makeText(RegisterActivity.this, throwable.getMessage(), Toast.LENGTH_LONG).show();

                            }
                        }));



            }
        });



        }// end of onCrate


    }

