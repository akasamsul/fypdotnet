package com.example.androidmvcnetcoreapiexample;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {


    public DatabaseHelper(@Nullable Context context) {
        super(context, "Login.db", null, 1);


    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE user(id INTEGER PRIMARY KEY AUTOINCREMENT,username TEXT,email TEXT ,password TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists user");
    }

    // inserting in database
    public long insert(String username,String email,String password){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("username",username);
        contentValues.put("email",email);
        contentValues.put("password",password);


//        Log.e("DatabaseHelper","dah masuk");
        long ins = db.insert("user",null,contentValues);

//        if(ins > 0)
//            return true;
//        else
//            return false;
        db.close();
        return ins ;
    }

    //checking if email exists;
    public Boolean chkemail(String email)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("Select * from user where email=?", new String[]{email});
        int count = cursor.getCount();
        cursor.close();
        db.close();

        if(cursor.getCount()>0)
            return false;
        else
            return true;

    }


    //check the username and password at login
    public boolean usernamepassword(String username, String password)
    {
     SQLiteDatabase db = this.getReadableDatabase();
     Cursor cursor = db.rawQuery("SELECT * FROM user where username=? and password =?", new String[]{username,password});

        if(cursor.getCount()>0)
            return true ;

            else
                return false ;



     }


}
