package com.example.androidmvcnetcoreapiexample.Remote;




import com.example.androidmvcnetcoreapiexample.Model.User;


import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface IMyAPI {

    //http://localhost:5000/api/login

    @POST("api/register")
    Observable<String> registerUser(@Body User user ) ;

    @POST("api/login")
    Observable<String> loginUser(@Body User user ) ;

}
