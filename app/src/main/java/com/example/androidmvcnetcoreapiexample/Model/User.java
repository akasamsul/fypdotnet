package com.example.androidmvcnetcoreapiexample.Model;

public class User {

    private int Id ;
    private String Name ;
    private String Email ;
    private String Password ;
    private String Salt ;

    public User(String name, String email, String password, String salt)
    {

        Name = name;
        Email = email;
        Password = password;
        Salt = salt;
    }
    public User(String name, String password, String salt)
    {

        Name = name;
        Password = password;
        Salt = salt;
    }
//    public User(String s, String toString, String string, String s1) {
//    }



    public User(int id, String name, String email, String password, String salt) {
        Id = id;
        Name = name;
        Email = email;
        Password = password;
        Salt = salt;
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getSalt() {
        return Salt;
    }

    public void setSalt(String salt) {
        Salt = salt;
    }
}
